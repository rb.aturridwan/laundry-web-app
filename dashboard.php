<?php
session_start();
require_once "database.php";
$pdo = new database();
$edit_form = false;

//Jika user belum login dan membuka ini, maka langsung diarahkan ke halaman login
if(isset($_SESSION['role']) != "admin"){
    header('Location: login.php');
}

//Jika admin login, maka langsung diarahkan ke halaman dashboard admin
//Ubah e-mailnya jika ingin mengganti akun admin
if($_SESSION['role'] == 'admin'){
    header('Location: admin');
}

if($_SESSION['role'] == 'kasir'){
  header('Location: kasir');
}

if($_SESSION['role'] == 'owner'){
  header('Location: owner');
}

//Memanggil tabel pesanan
$rows = $pdo -> getPesanan($_SESSION['user_id']);

//Mengambil data dan menaruh di kotak edit
if(isset($_GET['edit'])){
  $data = $pdo -> getEditPesanan($_GET['edit']);
  $edit_form = true;
  $name = $data['name'];
  $email = $data['email'];
  $nomor_telepon = $data['telepon'];
  $id = $data['id_user'];
}

//Mengupdate data
if(isset($_POST['update'])){
  $update = $pdo -> updateData($_POST['name'], $_POST['email'], $_POST['password'], $_POST['no_telepon'], $id);
  $_SESSION['name'] = $_POST['name'];
  $_SESSION['email'] = $_POST['email'];
  $_SESSION['nomor_telepon'] = $_POST['no_telepon'];
  // Cara get data dari spesifik name_order terus dikirim kesini
  header("Location: dashboard.php#profil");
}

//Untuk tombol membatalkan edit
if(isset($_POST['cancel'])){
  header("Location: dashboard.php#profil");
}

?>

<!DOCTYPE html>
<head>
    <title>Dashboard User - Lavender Laundry</title>
    <script src="js/jquery-3.5.1.js"></script>
    <script src="bootstrap/js/bootstrap.bundle.js"></script>
    <script src="bootstrap/jquery.dataTables.min.js"></script>
    <script src="bootstrap/dataTables.bootstrap4.min.js"></script>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/dash.css">
    <link rel="stylesheet" href="bootstrap/dataTables.bootstrap4.min.css">
</head>

<body>
    <!-- Navbar -->
<nav id="navbar-user" class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="dashboard.php">
  <img src="images/logo.png" style="height:50px"/>
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#beranda">Beranda</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="order.php">Order</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#status-order">Status Order</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#profil">Profil</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="logout.php">Logout</a>
      </li>
    </ul>
  </div>
</nav>

    <!-- Isi -->
<div data-spy="scroll" data-target="#navbar-user" data-offset="0">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 id ="beranda" class="display-4">Selamat datang, <?php echo $_SESSION['name']; ?>!</h1>
            <br>
            <p class="lead">Silahkan order atau cek status laundry anda disini.</p>
            <br>
            <a class="btn btn-outline-success active" href="order.php"><b>ORDER DISINI</b></a>
        </div>
    </div>
    <div class="jumbotron jumbotron-fluid bg-white" style="font-size:12px">
        <div class="container">
            <h1 class="tengah" id ="status-order">Status Order</h1>
            <p class="tengah">Cek status laundry anda disini.</p>
            <br>
            <table id="pagination" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Jenis Laundry</th>
                        <th scope="col">Rincian</th>
                        <th scope="col">Massa/Jumlah Barang</th>
                        <th scope="col">Jenis Pengambilan</th>
                        <th scope="col">Tanggal Laundry/ Dijemput</th>
                        <th scope="col">Tanggal Selesai/ Diantar</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Harga Total</th>
                        <th scope="col">Status Laundry</th>
                        <th scope="col">Status Bayar</th>
                        <th scope="col">Metode</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                        foreach ( $rows as $row ) {
                    ?>
                    <tr>
                        <td><b><?=$row['jenis_laundry'] ?></b></td>
                        <td><?=$row['jenis_laundry'] ?></td>
                        <td><?=$row['massa_barang'] > 0 ? $row['massa_barang'].' kg' : "-" ?> </td>
                        <td><?=$row['jenis_pengambilan'] ?></td>
                        <td><?=$row['tgl'] ?></td>
                        <td><?=$row['batas_waktu'] ?></td>
                        <td><?=$row['alamat'] ?></td>
                        <td>Rp.<?=$row['harga_total'] ?></td>
                        <td><b><?=$row['status'] == "baru" ? "Sedang Diproses" : $row['status'] ?></b></td>
                        <td><b><?=$row['status_bayar'] == "dibayar" ? "Sudah Dibayar" : "Belum Dibayar" ?></b></td>
                        <td><b><?=$row['metode_bayar'] == "cod" ? "COD" : $row['metode_bayar'] == "transfer" ? "Transfer" : "DANA" ?></b></td>
                        <?php if($row['metode_bayar'] != 'cod' && $row['status_bayar'] != "dibayar"){ ?>
                        <td><a href="confirm-payment.php?id=<?=$row['id_transaksi'];?>" class="btn btn-sm btn-success">Upload Bukti Bayar</a></td>
                        <?php } else {?>
                        <td>-</td>
                        <?php } ?>
                    </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="jumbotron jumbotron-fluid bg-dark text-light">
        <div class="container">
            <h1 class="tengah" id ="profil">Informasi Profil</h1>
            <p class="tengah">Informasi tentang name, E-mail, dan Nomor Telepon yang digunakan.</p>
            <br>
            <ul>
                <li>name : <?php echo ($_SESSION['name']) ?></li>
                <br>
                <li>E-mail : <?php echo ($_SESSION['email']) ?> </li>
                <br>
                <li>Nomor Telepon : <?php echo ($_SESSION['nomor_telepon']) ?> </li>
            </ul>
            <br> 
            <form action="dashboard.php?edit=<?php echo $_SESSION['user_id']; ?>#profil" method="post">
              <input type="hidden" name="id" value="<?=$_SESSION['user_id']?>">
              <input type="submit" value="Edit" name="edit">
            </form>
          
            <?php 
    if($edit_form == false){ ?>
        <!-- Kosong -->
    <?php
    } 
    else{ ?>
        <!-- Untuk memunculkan edit profil -->
        <h4 class="tengah">Edit profil</h4>
        <form method="post">
            <p class="tengah">name : </p>
            <p class="tengah"><input type="text" class = "tengah" name="name" value="<?php echo $name; ?>"></p>
        <p class="tengah">E-mail : </p>
            <p class="tengah"><input type="email" class = "tengah" name="email" value="<?php echo $email; ?>"></p>
        <p class="tengah">Password : </p>
            <p class="tengah"><input type="password" class = "tengah" name="password" id="password" pattern=".{8,}" required title="Minimum 8 karakter" placeholder="Masukkan password"></p>
            <p class="tengah"><input type="checkbox" onclick="myFunction()"> Show Password</p>
        <p class="tengah">Nomor Telepon : </p>
            <p class="tengah"><input type="text" class="tengah" name="no_telepon" value="<?php echo $nomor_telepon; ?>" pattern=".{10,14}" required></p>
        
    <?php
    } ?>

<?php 
    if($edit_form == false){ ?>
    <?php
    } 
    else{ ?>
        <p class="tengah"><input type="submit" name="update" value="Update"/>
        <input type="button" onclick="location.href='dashboard.php#profil';" value="Cancel" />
        </p>
        </form>
    <?php
    } ?>
    </div>
</div>


<!-- Footer -->
<footer class="page-footer font-small blue">
  <div class="footer-copyright text-center py-3 bg-dark text-white">© 2023 Copyright:
    <a href="#"> Lavender Laundry</a>
  </div>
</footer>

</div>
</div>
</body>
<script>
    $(document).ready(function() {
    $('#pagination').DataTable();
} );

//Memunculkan password
function myFunction(){
        var x = document.getElementById("password");
        if (x.type === "password"){
            x.type = "text";
        } 
        else{
            x.type = "password";
        }
    }
</script>