<?php

session_start();
require_once "database.php";
// membuat objek
$pdo = new database();

//Jika user belum login dan membuka halaman order, maka langsung diarahkan ke halaman login
if(isset($_SESSION['email']) == 0){
    header('Location: login.php');
}

//Jika admin login, maka langsung diarahkan ke halaman dashboard admin
//Ubah e-mailnya jika ingin mengganti akun admin
if($_SESSION['role'] == 'admin'){
    header('Location: admin');
}

$id = $_POST['id'];

// File upload path
$targetDir = "uploads/";
$fileName = basename($_FILES["file"]["name"]);
$targetFilePath = $targetDir . $fileName;
$fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);

if(isset($_POST["submit"]) && !empty($_FILES["file"]["name"])){
	// Allow certain file formats
	$allowTypes = array('jpg','png','jpeg','pdf');
	if(in_array($fileType, $allowTypes)){

        if(! is_dir($targetDir)){
            mkdir($targetDir);
        }
		// Upload file to server
		if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
			// Insert image file name into database
			$sql = $pdo->uploadBuktiBayar($fileName, $id);
			if($sql){
				$statusMsg = "The file ".$fileName. " has been uploaded successfully.";
			}else{
				$statusMsg = "File upload failed, please try again.";
			} 
		}else{
			$statusMsg = "Sorry, there was an error uploading your file.";
		}
	}else{
		$statusMsg = 'Sorry, only JPG, JPEG, PNG, & PDF files are allowed to upload.';
	}
}else{
	$statusMsg = 'Please select a file to upload.';
}

// Display status message
echo "<script type='text/javascript'>alert('$statusMsg');window.location.href='dashboard.php';</script>";




?>