<?php
session_start();
require_once "database.php";
// membuat objek
$pdo = new database();

$id = $_GET['id'];
$userId = $_SESSION['user_id'];


if(!isset($id) && !isset($userId)){
	header('Location: dashboard.php');
}


$rows = $pdo -> getPesananDetail($userId, $id);

if(!isset($rows) OR count($rows) < 1){
	header('Location: dashboard.php');
}

//Jika user belum login dan membuka halaman order, maka langsung diarahkan ke halaman login
if(isset($_SESSION['email']) == 0){
    header('Location: login.php');
}

//Jika admin login, maka langsung diarahkan ke halaman dashboard admin
//Ubah e-mailnya jika ingin mengganti akun admin
if($_SESSION['role'] == 'admin'){
    header('Location: admin');
}

?>

<!DOCTYPE html>
<html lang="id">
	<head>
		<title>Pemesanan Laundry</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" type="text/css" href="css/order.css"/>
		<link rel="stylesheet" type="text/css" href="css/roboto-font.css">
		<link rel="stylesheet" type="text/css" href="fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
		<script src="js/jquery-3.3.1.min.js"></script>
		<script src="js/jquery.steps.js"></script>
		<script src="js/jquery-ui.min.js"></script>
		<script src="js/order.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKtmUDqFDJ8-D3F0nJM4bpiD4hAR-fzeo"></script>
	</head>
	<body>
		<div class="page-content" style="background-image: url('images/laundry2.jpg');background-attachment:fixed">
			<div class="pemesanan" style="padding:0 30px">
				<div class="order-form">
					<div class="order-header">
						<h3 class="heading">Lavender Laundry</h3>
						<p>Harap Mengisi Semua Data Yang Dibutuhkan</p>
					</div>
					<div class="inner">
						<h3>Metode Pembayaran:</h3>
						<div class="form-row table-responsive">
							<table class="table" style="vertical-align:middle">
								<tbody>
									<?php if($rows[0]['metode_bayar'] == "transfer") { ?>
									<tr class="space-row">
										<th class="space-row">
											<span class="label">Transfer</span>
											<div class="bca" style="display">
												<img src="assets/img/bcalogo.png" alt="" style="width:150px;padding:20px" />
												<div style="padding:0 15px">0670576868 - Febriyani Nenobais</div>
											</div>
										</th>
									</tr>
									<?php }?>
									<?php if($rows[0]['metode_bayar'] == "dana") { ?>

									<tr class="space-row">
										<th class="space-row">
											<img src="assets/img/dana.png" alt="" style="height:30px" />
											<div class="qr-dana" style="display">
												<img src="assets/img/qrdana.jpeg" alt="" style="width:250px" />
											</div>
										</th>
									</tr>
									<?php }?>

								</tbody>
							</table>
						</div>
					</div>
					<form action="upload.php" method="post" enctype="multipart/form-data">
						<section>
							<div class="inner">
								<h3>Silahkan upload bukti pembayaran anda</h3>
								<input type="hidden" name="id" value="<?= $id;?>">

								<div class="form-row">
									<div class="" style="display:flex;padding:0px 20px 40px 20px">
									<label class="form-row-inner">
										<input type="file" name="file">
										<input type="submit" name="submit" value="Upload">
									</label>
									</div>
								</div>
							</div>
						</section>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>